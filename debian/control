Source: r-other-ascat
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
#Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-rcolorbrewer,
               r-cran-data.table,
               r-bioc-genomicranges,
               r-bioc-iranges,
               r-cran-doparallel,
               r-cran-foreach
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-other-ascat
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-other-ascat.git
Homepage: https://github.com/Crick-CancerGenomics/ascat
Rules-Requires-Root: no

Package: r-other-ascat
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: Allele-Specific Copy Number Analysis of Tumours
 ASCAT (allele-specific copy number analysis of tumors) is a allele-
 specific copy number analysis of the in vivo breast cancer genome. It
 can be used to accurately dissect the allele-specific copy number of
 solid tumors, simultaneously estimating and adjusting for both tumor
 ploidy and nonaberrant cell admixture.
